import axios from 'axios'
import React from 'react'
import {useSelector,useDispatch} from 'react-redux'
import {getData} from './actions'
import {storeData} from './actions'
import {deleteData} from './actions'
import {getPostData} from './actions'

export default function UserData () 
{
   const userData = useSelector(state => state.userdata)
   const userInfo = useSelector(state => state.uerInfo)
   const [postdata,setPostdata] = React.useState([])
   const dispatch = useDispatch();
   const getUserData = () =>
   {
      axios.get("https://jsonplaceholder.typicode.com/posts")
      .then((res) =>
      {
        setPostdata(res.data)
        dispatch(getData())
        dispatch(storeData(res.data))
        dispatch(getPostData())
      })
  }
  const removeData = (value) =>
    {
        const tmp=dispatch(deleteData(value))
                      
    }
  React.useEffect(() => {
    getUserData()
  },[])
    return (
    <div className="App">
        
          <h1>Users data</h1>
      
           <table> 
           {userData.map((value,index)=>{
              return  <tr key={index} style={{color:'#000000'}}>
                  <td>{value.id} </td>
                  <td>{value.artist}</td>
                  <td>{value.album.title} </td>
                
                  </tr>
            }
            
            )}

                
               </table> 
               <h1>Posts data</h1>
               <table> 
{userInfo.map((value,index)=>{
              return  <tr key={index} style={{color:'#000000'}}>
                   <td>{value.id}</td>
                  <td>{value.title}</td>
                 
                  <td><button onClick={()=> removeData(value) }>Delete</button></td>
                  </tr>
            }
            
            )}
       </table> 

    </div>
    );
}