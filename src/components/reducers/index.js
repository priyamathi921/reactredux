import GetDataReducer from './GetData'
import PostDataReducer from './PostData'
import {combineReducers} from 'redux'

const allReducers = combineReducers({
    userdata: GetDataReducer,
    uerInfo: PostDataReducer
});

export default allReducers;