export const getData = () =>
{
  return {
    type:'FETCH_DATA'
  }
}
export const storeData = (data) =>
{
  return {
    type:'STORE_DATA',
    payload: data
  }
}

export const deleteData = (data) =>
{
  return {
    type:'DELETE',
    payload: data
  }
}
  export const getPostData = () =>
{
  return {
    type:'FETCH_POST_DATA'
  }
}

